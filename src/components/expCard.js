import React from 'react'
import { expcard } from '../services/data'

export default function ExpCard() {
    let data = expcard
    return (
        
            data.map(obj => 
            <div className="col-sm px-3">
                <h6>{obj.title}</h6>
                <p>{obj.text}</p>
            </div>
            )


            
        
    )
}
