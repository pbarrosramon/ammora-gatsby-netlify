import React from 'react'
import Img from "gatsby-image"
import { useStaticQuery, graphql } from "gatsby"

export default function FluidBlock(opt) {
    return (
        <div className="container-fluid bg-primary">
            <div className="container py-5">
                <div className="row py-5">
                    <div className="col-md">
                        <h1>{opt.title}</h1>
                    </div>
                    <div className="col-md">
                        <Img fluid={opt.gbimg}/>
                    </div>
                </div>
                {opt.children}
            </div>
        </div>
    )
}
