import React from 'react'
import Img from "gatsby-image"
import { showcase } from "../services/data"

export default function ShowCase(opt) {

    const data = showcase;

    return (
        data.map(obj =>
            obj.inverse? (
                <div className="row align-items-center justify-content-center">
                    <div className="m-5 col-4">
                        <h6>{obj.title}</h6>
                        <h3>{obj.subtitle}</h3>
                        <h6>{obj.text}</h6>
                    </div>
                <div className={"m-5 col-4 rounded " + obj.bgColorClass}>
                    <center>
                        <img 
                        src={obj.gbimg}
                        style={{maxWidth: "200px"}}
                        />
                    </center>
                </div>
            </div>
            ):
            (
                <div className="row align-items-center justify-content-center">
                    <div className={"m-5 col-4 rounded " + obj.bgColorClass}>
                        <center>
                            <img 
                            src={obj.gbimg}
                            style={{maxWidth: "200px"}}
                            />
                        </center>
                    </div>
                    <div className="m-5 col-4">
                        <h6>{obj.title}</h6>
                        <h3>{obj.subtitle}</h3>
                        <h6>{obj.text}</h6>
                    </div>
                </div>
            )
        )
    )
}
