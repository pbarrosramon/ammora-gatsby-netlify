import React from 'react'
import { Link } from "gatsby"

export default function MenuElem(opt) {
    return (
        <li className="nav-item px-3">
            <Link to={opt.to}>
                {opt.children}
            </Link>
        </li>
    )
}