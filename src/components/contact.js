import React from 'react'

export default function Contact() {
    return (
        <div className="container mt-5">
      <div className="row">
        <div className="col-lg">
          <div> 
            <h2>LET'S GET IN TOUCH</h2>
          </div>
          <div className="mt-3">
            <h6 >Talk about your great idea</h6><br></br>
          </div>
          <div className="mt-2">
            <h6>ADDRESS</h6>
            <h6>AV PAU BRASIL,6 SALA 1910</h6>
            <h6>AGUAS CLARAS - DF</h6>
          </div>
          <div className="mt-3">
            <h6>PHONE</h6>
            <p>+55 61 9999-0000</p>
          </div>
        </div>
        <div className="col-lg form-group">
          <form>
            <div className="input-group">
              <input className="form-control mr-2" type="text" name="name" placeholder="Your Name"></input>
              <input className="form-control ml-2" type="email" name="email" placeholder="Your Email"></input>
            </div>
            <div className="mt-4">
              <textarea className="form-control w-100" rows="5" name="message" placeholder="Your Message"></textarea>
              <input className="btn btn-primary mt-2" type="submit" name="enviar"></input>
            </div>
          </form>
        </div>
      </div>
    </div>
    )
}
