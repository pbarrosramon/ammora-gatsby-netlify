import React from 'react'
import Img from "gatsby-image"
import { useStaticQuery, graphql } from "gatsby"
import SEO from "./seo"

export default function TitleBlock() {
    
    const images = useStaticQuery(graphql`
    query {
        girlstwo: file(relativePath: { eq: "girls2.png" }) {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid
              }
            }
          }
          robot: file(relativePath: { eq: "robot.png" }) {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
    `)

    return (
        <div>
            <SEO title="Home Ammora"></SEO>

            <div className="container">
                <div className="row pt-5">
                    <div className="col-md">
                        <h1>Let's create something bigger together</h1>
                        <p>Development and infrastructure for digital amazing products.</p>
                    </div>
    
                    <div className="col-md align-self-center">
                        <Img fluid={images.girlstwo.childImageSharp.fluid}/>
                    </div>
                </div>
    
                <div className="row pt-5">
                    <div className="col-md">
                        <button className="btn btn-primary">Hire us</button>
                        <button className="btn btn-outline-dark ml-4">Our work</button>
                    </div>
                </div>
    
            <div className="row py-5">
                <div className="col-sm-1">
                     <Img fluid={images.robot.childImageSharp.fluid}/>
                </div>
                <div className="col align-self-center">
                    <p>Hi, can I help you?</p>
                </div>
            </div>
        </div>
    </div>
    )
}
