import React from 'react'
import { Link } from "gatsby"

function BgPrimary({opt}){
    return (
        <div  className="bg-primary col-4 align-items-center py-5">
            <center><h6 className="text-center">{opt.title}</h6></center>
            <h1 style={{padding: "0 0 15rem 0"}} className="text-center">{opt.text}</h1>
            <Link to={opt.link}><center><button className="btn btn-light mt-5">{opt.buttonText}</button></center></Link>
        </div>
    )
}
function NoBg({opt}){
    return (
        <div  className="col-4 align-items-center py-5">
            <center><h6 className="text-center">{opt.title}</h6></center>
            <h1 style={{padding: "0 0 15rem 0"}} className="text-center">{opt.text}</h1>
            <Link to={opt.link}><center><button className="btn btn-outline-primary mt-5">{opt.buttonText}</button></center></Link>
        </div>
    )
}

export default function CTA(opt) {
    return opt.bgstyle? <BgPrimary opt={opt}/> : <NoBg opt={opt}/>
    
}
