import React from 'react'
import Img from "gatsby-image"
import { useStaticQuery, graphql } from "gatsby"

export default function Footer() {
  const images = useStaticQuery(graphql`
  query {
    whiteammora: file(relativePath: { eq: "whiteammora.png" }) {
      childImageSharp {
        fixed(width: 135) {
          ...GatsbyImageSharpFixed
        }
      }
    }
  }`
  )
    return (
        <footer>
          <div className="container-fluid bg-dark text-light">
            <div className="container">
              <div className="row py-5">
                <div className="col">
                  Subscribe and receive our awesome news
                </div>
                <div className="col align-item-center">
                  <center><Img fixed={images.whiteammora.childImageSharp.fixed}/></center>
                </div>
                <div className="col form-group">
                  <input className="form-control" type="email" name="email" placeholder="Your Email"></input>
                </div>
              </div>
            </div>
          </div>
        </footer>
    )
}
