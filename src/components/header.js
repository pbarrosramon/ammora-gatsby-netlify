//import { Link } from "gatsby"
import { imgquery } from "../services/imgquery"
import { useStaticQuery, graphql } from 'gatsby'
import PropTypes from "prop-types"
import React from "react"
import Img from "gatsby-image"
import MenuElem from "./menuElem"

const Header = () => {
  const query = useStaticQuery(graphql`
query {
  one: file(relativePath: { eq: "1.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  two: file(relativePath: { eq: "2.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  three: file(relativePath: { eq: "3.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  bancobrasil: file(relativePath: { eq: "bancobrasil.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  bankapp: file(relativePath: { eq: "bankapp.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  caixaeconomica: file(relativePath: { eq: "caixa.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  fenae: file(relativePath: { eq: "fenae.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  flightapp: file(relativePath: { eq: "flightapp.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  girlstwo: file(relativePath: { eq: "girls2.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  iphonex: file(relativePath: { eq: "iphonex.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  logo: file(relativePath: { eq: "logo.png" }) {
    childImageSharp {
      fluid(maxWidth: 300) {
        ...GatsbyImageSharpFluid
      }
    }
  }
  macbookzerotwo: file(relativePath: { eq: "macbook02.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  neojets: file(relativePath: { eq: "neojets.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  robot: file(relativePath: { eq: "robot.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  senado: file(relativePath: { eq: "senado.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  ticketapp: file(relativePath: { eq: "ticketapp.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
}
`)
console.log(query)
  const logo = query.logo.childImageSharp.fluid
  

  return(
  <header>
    <div className="container">
      <div className="menubar row pt-3 px-5 align-items-start">
        <div className="col">
          <Img className="img-fluid w-25" fluid={logo}/>
        </div>
      
        <div className="col">
          <nav>
          <ul className="nav justify-content-end">
              <MenuElem to="/products">Products</MenuElem>  
              <MenuElem to="/about">About</MenuElem>
              <MenuElem to="/works">Works</MenuElem>
              <MenuElem to="/hireus">Hire us</MenuElem>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </header>)
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
