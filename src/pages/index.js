import React from "react"
//import { Link } from "gatsby"
import imgquery from "../services/imgquery"

import { useStaticQuery, graphql } from 'gatsby'
import Img from "gatsby-image"
import Layout from "../components/layout"
import TitleBlock from "../components/titleBlock"
import FluidBlock from "../components/fluidBlock"
import ExpCard from "../components/expCard" 
import ShowCase from "../components/showCase"
import CTA from "../components/cta"
import Contact from "../components/contact"

const IndexPage = () => {
  const images = useStaticQuery(graphql`
  query {
  one: file(relativePath: { eq: "1.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  two: file(relativePath: { eq: "2.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  three: file(relativePath: { eq: "3.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  bancobrasil: file(relativePath: { eq: "bancobrasil.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  bankapp: file(relativePath: { eq: "bankapp.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  caixaeconomica: file(relativePath: { eq: "caixa.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  fenae: file(relativePath: { eq: "fenae.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  flightapp: file(relativePath: { eq: "flightapp.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  girlstwo: file(relativePath: { eq: "girls2.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  iphonex: file(relativePath: { eq: "iphonex.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  logo: file(relativePath: { eq: "logo.png" }) {
    childImageSharp {
      fluid(maxWidth: 300) {
        ...GatsbyImageSharpFluid
      }
    }
  }
  macbookzerotwo: file(relativePath: { eq: "macbook02.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  neojets: file(relativePath: { eq: "neojets.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  robot: file(relativePath: { eq: "robot.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  senado: file(relativePath: { eq: "senado.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
  ticketapp: file(relativePath: { eq: "ticketapp.png" }) {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid
      }
    }
  }
}
`
  )

  return(
  <Layout>
    <TitleBlock/>
      <FluidBlock 
        title="Our approach is assembling a dedicated development team that aims your business to the highly competitive market"
        gbimg={images.macbookzerotwo.childImageSharp.fluid}>
        <div className="row py-5">
          <ExpCard/>
        </div>
    </FluidBlock>

    <div className="container py-5">
      <div className="row justify-content-center pb-5">
        <div className="col-4">
          <h1 className="text-center">Our Work</h1>
        </div>
      </div>
      <ShowCase/>
    </div>
    
    <div className="container py-5">
      <div>
        <div className="row justify-content-center py-2">
          <div className="col-12">
          <h1 className="text-center">Some of our amazing clients</h1>
          </div>
        </div>
        <div className="row align-items-center justify-content-center py-3">
          <div className="col"><Img fluid={images.caixaeconomica.childImageSharp.fluid}/></div>
          <div className="col"><Img fluid={images.neojets.childImageSharp.fluid}/></div>
          <div className="col"><Img fluid={images.bancobrasil.childImageSharp.fluid}/></div>
          <div className="col"><Img fluid={images.fenae.childImageSharp.fluid}/></div>
          <div className="col"><Img fluid={images.senado.childImageSharp.fluid}/></div>
        </div>
      </div>
    </div> 

    <div className="container py-5">
      <div className="row justify-content-center">
        <CTA
          title="Design Sprint"
          text="I need help to start my project"
          link="#"
          buttonText="START HERE"
        />
        <CTA
          bgstyle
          title="Full Development"
          text="I know what I need"
          link="#"
          buttonText="START HERE" 
        />
      </div>
      <div className="row justify-content-center mt-5 py-5">
        <div className="col-8 align-items-center">
          <h3 className="text-center">We are ready to build your next successfull project</h3>
        </div>
      </div>
    </div>
    <Contact/>
  </Layout>
  )
}

export default IndexPage
