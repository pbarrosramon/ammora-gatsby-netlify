//import imgquery from "../services/imgquery"

const expcard = [
    
    {
        title:"1. Top engineering talents", 
        text:"Anim voluptate dolore eu labore nostrud veniam aliqua cillum elit magna."
    },
    {
        title:"2. Innovative software solutions", 
        text:"Anim voluptate dolore eu labore nostrud veniam aliqua cillum elit magna."
    },
    {
        title:"3. Business analysis and UI/UX design",
        text:"Anim voluptate dolore eu labore nostrud veniam aliqua cillum elit magna."
    }
];

const showcase = [
    {
        inverse: false,
        bgColorClass: "bg-primary",
        gbimg: "/static/6cc3e5078f812090cef109db22019de1/7990b/ticketapp.png",
        title: "DEVOPS",
        subtitle: "The mobile payment service is running",
        text: "The U.S. has succeeded in embedding",
    },
    {
        inverse: true,
        bgColorClass: "bg-info",
        gbimg: "/static/fcca0f9bf99b218c541e117950c2bca0/7990b/bankapp.png",
        title: "FRONTEND",
        subtitle: "The mobile payment service is running",
        text: "The U.S. has succeeded in embedding",
    },
    {
        inverse: false,
        bgColorClass: "bg-warning",
        gbimg: "/static/9c82cd01409515ee2955ada72789b36c/3d72d/flightapp.png",
        title: "DEVOPS",
        subtitle: "The mobile payment service is running",
        text: "The U.S. has succeeded in embedding"
    }
]

export { expcard, showcase }