const imgquery = `
{
one: file(relativePath: { eq: "1.png" }) {
  childImageSharp {
    fluid {
      ...GatsbyImageSharpFluid
    }
  }
}
two: file(relativePath: { eq: "2.png" }) {
  childImageSharp {
    fluid {
      ...GatsbyImageSharpFluid
    }
  }
}
three: file(relativePath: { eq: "3.png" }) {
  childImageSharp {
    fluid {
      ...GatsbyImageSharpFluid
    }
  }
}
bancobrasil: file(relativePath: { eq: "bancobrasil.png" }) {
  childImageSharp {
    fluid {
      ...GatsbyImageSharpFluid
    }
  }
}
bankapp: file(relativePath: { eq: "bankapp.png" }) {
  childImageSharp {
    fluid {
      ...GatsbyImageSharpFluid
    }
  }
}
caixaeconomica: file(relativePath: { eq: "caixa.png" }) {
  childImageSharp {
    fluid {
      ...GatsbyImageSharpFluid
    }
  }
}
fenae: file(relativePath: { eq: "fenae.png" }) {
  childImageSharp {
    fluid {
      ...GatsbyImageSharpFluid
    }
  }
}
flightapp: file(relativePath: { eq: "flightapp.png" }) {
  childImageSharp {
    fluid {
      ...GatsbyImageSharpFluid
    }
  }
}
girlstwo: file(relativePath: { eq: "girls2.png" }) {
  childImageSharp {
    fluid {
      ...GatsbyImageSharpFluid
    }
  }
}
iphonex: file(relativePath: { eq: "iphonex.png" }) {
  childImageSharp {
    fluid {
      ...GatsbyImageSharpFluid
    }
  }
}
logo: file(relativePath: { eq: "logo.png" }) {
  childImageSharp {
    fluid(maxWidth: 300) {
      ...GatsbyImageSharpFluid
    }
  }
}
macbookzerotwo: file(relativePath: { eq: "macbook02.png" }) {
  childImageSharp {
    fluid {
      ...GatsbyImageSharpFluid
    }
  }
}
neojets: file(relativePath: { eq: "neojets.png" }) {
  childImageSharp {
    fluid {
      ...GatsbyImageSharpFluid
    }
  }
}
robot: file(relativePath: { eq: "robot.png" }) {
  childImageSharp {
    fluid {
      ...GatsbyImageSharpFluid
    }
  }
}
senado: file(relativePath: { eq: "senado.png" }) {
  childImageSharp {
    fluid {
      ...GatsbyImageSharpFluid
    }
  }
}
ticketapp: file(relativePath: { eq: "ticketapp.png" }) {
  childImageSharp {
    fluid {
      ...GatsbyImageSharpFluid
    }
  }
}
}
`

export default imgquery